#!/bin/bash
BASE_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

#REPO=registry.gitlab.com/docker-master/ngino_proxy
REPO=gregorwebmaster/ngino_proxy
LATEST=1.21

function push-images() {
	docker push ${REPO}:${1:-latest}
}

function build-images() {
	docker build -t ${REPO}:${1:-latest} -f $BASE_DIR/docker/${1:-$LATEST}/Dockerfile $BASE_DIR
}

case $1 in
b | build)
	build-images $2
	;;
p | push)
	push-images $2
	;;
l | list)
	docker image ls | grep $REPO
	;;
code)
	code $BASE_DIR
esac